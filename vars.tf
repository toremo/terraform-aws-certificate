variable "region" {
  description = "Region where the certificate needs to be created"
}

variable "domain_name" {
  description = "Name of the domain of the certificate"
}

variable "zone_id" {
  description = "The ID of the zone where the verification record will be created"
}
