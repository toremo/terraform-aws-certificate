provider "aws" {
  region = "${var.region}"
}

resource "aws_acm_certificate" "default" {
  domain_name       = "${var.domain_name}"
  validation_method = "DNS"
}

resource "aws_route53_record" "default_validation" {
  name     = "${aws_acm_certificate.default.domain_validation_options.0.resource_record_name}"
  type     = "${aws_acm_certificate.default.domain_validation_options.0.resource_record_type}"
  records  = ["${aws_acm_certificate.default.domain_validation_options.0.resource_record_value}"]
  zone_id  = "${var.zone_id}"
  ttl      = 60
}

resource "aws_acm_certificate_validation" "default" {
  certificate_arn         = "${aws_acm_certificate.default.arn}"
  validation_record_fqdns = ["${aws_route53_record.default_validation.fqdn}"]
}