# README
This module creates a TLS certificate for the domain provided as a parameter.
It uses a DNS verification method; for this reason the caller needs to provide the zone_id of the hosted zone.